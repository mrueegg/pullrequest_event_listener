AJS.toInit(function() {
  //var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
  var baseUrl = AJS.contextPath()

  function populateForm() {
    AJS.$.ajax({
      url: baseUrl + "/rest/stash-event-listener/1.0/",
      dataType: "json",
      success: function(config) {
        AJS.$("#bamboo").attr("value", config.bamboo);
        AJS.$("#repo").attr("value", config.repo);
        AJS.$("#user").attr("value", config.user);
        AJS.$("#pass").attr("value", config.pass);
      }
    });
  }
  function updateConfig() {
    AJS.$.ajax({
      url: baseUrl + "/rest/stash-event-listener/1.0/",
      type: "PUT",
      contentType: "application/json",
      data: '{ "bamboo": "' + AJS.$("#bamboo").attr("value") + '", "repo": "' +  AJS.$("#repo").attr("value").replace(/\"/g, "\\\"").replace(/\n/g, "\\n") + '", "user": "' +  AJS.$("#user").attr("value") + '", "pass": "' +  AJS.$("#pass").attr("value") + '"}',
      processData: false
    });
  }
  populateForm();

  AJS.$("#admin").submit(function(e) {
    e.preventDefault();
    updateConfig();
  });
});