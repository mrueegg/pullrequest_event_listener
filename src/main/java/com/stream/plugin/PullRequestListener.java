package com.stream.plugin;


import com.atlassian.bitbucket.event.pull.PullRequestEvent;
import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestAction;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PullRequestListener implements InitializingBean, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(PullRequestListener.class);
    private final EventPublisher eventPublisher;
    private final PluginSettings settings;
    private final String pluginName = ConfigResource.pluginName;
    private URL bambooUrl;
    private String bambooUser;
    private String bambooPass;
    private ArrayList<RepositoryConfig> repositoryConfigs;
    private RepositoryConfig current_config;

    private String projectKey = "API";
    private ArrayList<String> repositoryNames = new ArrayList<String>();

    public PullRequestListener(EventPublisher eventPublisher, PluginSettingsFactory pluginSettingsFactory) throws IOException {
        this.settings = pluginSettingsFactory.createGlobalSettings();
        this.eventPublisher = eventPublisher;
    }

    private void getConfigs() throws IOException {
        // Bamboo URL config
        try {
            bambooUrl = new URL((String) this.settings.get(pluginName + ".bamboo"));
        } catch (MalformedURLException e) {
            log.error(String.valueOf(e));
        }

        // Bamboo User config
        bambooUser = (String) this.settings.get(pluginName + ".user");

        // Bamboo Password config
        bambooPass = (String) this.settings.get(pluginName + ".pass");

        // Repository configs
        String repoConfig = (String) this.settings.get(pluginName + ".repo");
        List<String> configs = Arrays.asList(repoConfig.split("\n"));
        ObjectMapper mapper = new ObjectMapper();
        this.repositoryConfigs = new ArrayList<RepositoryConfig>();
        for (String element : configs) {
           this.repositoryConfigs.add(mapper.readValue(element, RepositoryConfig.class));
        }
    }

    @Override
    public void destroy() throws Exception {
       eventPublisher.unregister(this);
       log.info("Pull Request Event handler is stopped");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
        log.info("Pull Request Event handler is started");
    }

    @EventListener
    public void onPullRequestEvent(PullRequestEvent pullRequestEvent) throws IOException {
        log.debug("Got pull request event");
        PullRequest pullRequest = pullRequestEvent.getPullRequest();
        PullRequestAction action = pullRequestEvent.getAction();
        String repositoryName = pullRequest.getFromRef().getRepository().getName();
        String projectId = pullRequest.getFromRef().getRepository().getProject().getKey();

        log.debug("PullRequest Action is {}", pullRequestEvent.getAction());
        log.debug("PullRequest Id is {}", pullRequest.getId());
        log.debug("PullRequest Project Key is {}", projectId);
        log.debug("PullRequest Repository Name is {}", repositoryName);

        // Find appropriate config
        getConfigs();
        for (RepositoryConfig config : this.repositoryConfigs) {
            if (config.getProjectId().equals(projectId)) {
                if (config.getRepositoryName().equals(repositoryName)) {
                    this.current_config = config;
                }
            }
        }

        if (this.current_config == null) {
            return;
        }

        if ((PullRequestAction.OPENED == action ||
                PullRequestAction.RESCOPED == action ||
                PullRequestAction.REOPENED == action)){

            String branchName = pullRequest.getFromRef().getDisplayId();

            log.info("Got pull request event [{}] for {} repository", action, branchName);

            if (PullRequestAction.RESCOPED == action) {
                String previousFromHash = ((PullRequestRescopedEvent) pullRequestEvent).getPreviousFromHash();
                String currentFromHash = pullRequestEvent.getPullRequest().getFromRef().getLatestCommit();
                if (previousFromHash.equals(currentFromHash)) {
                    log.debug("{} branch was not modified", branchName);
                    return;
                }
            }

            BambooApiClient bambooApiClient = new BambooApiClient(bambooUrl, bambooUser, bambooPass);
            BambooResponseParser bambooResponseParser = new BambooResponseParser();
            try {
                String payload = bambooApiClient.getBranchesForBuild(this.current_config.getBuildKey());
                String buildKey = bambooResponseParser.getBranchBuildKey(payload, branchName);
                bambooApiClient.triggerBambooBuild(buildKey);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
    }
}
