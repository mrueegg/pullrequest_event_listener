package com.stream.plugin;

import org.dom4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class BambooResponseParser {

    private static final Logger log = LoggerFactory.getLogger(BambooResponseParser.class);
    String branchBuildKey;

    public String getBranchBuildKey(String payload, String branchName) throws DocumentException {
        try {
            Document doc = DocumentHelper.parseText(payload);
            Node node = doc.selectSingleNode(String.format("//branch[@shortName='%s']", branchName));
            Element elem = (Element) node;
            branchBuildKey = elem.attributeValue("key");
        } catch (DocumentException e) {
            log.error("Can't parse response from Bamboo server");
        }
        return branchBuildKey;
    }
}
