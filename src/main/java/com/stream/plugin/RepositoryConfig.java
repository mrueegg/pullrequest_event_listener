package com.stream.plugin;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RepositoryConfig {

    private String projectId;
    private String repositoryName;
    private String buildKey;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRepositoryName() {
        return this.repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getBuildKey() {
        return buildKey;
    }

    public void setBuildKey(String buildKey) {
        this.buildKey = buildKey;
    }
}
