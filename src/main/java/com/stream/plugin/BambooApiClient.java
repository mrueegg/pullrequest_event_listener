package com.stream.plugin;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;

public class BambooApiClient {

    private static String baseUrl;
    private static CredentialsProvider credentialsProvider;
    private static HttpClientContext context;

    private static final Logger log = LoggerFactory.getLogger(BambooApiClient.class);

    public BambooApiClient(URL bambooUri, String bambooUser, String bambooPass) {
        String host = bambooUri.getHost();
        Integer port = bambooUri.getPort();
        baseUrl = String.format("https://%s/rest/api/latest/", host, port);

        HttpHost targetHost = new HttpHost(host, port, "https");
        credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(
                AuthScope.ANY,
                new UsernamePasswordCredentials(bambooUser, bambooPass));

        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

        context = HttpClientContext.create();
        context.setCredentialsProvider(credentialsProvider);
        context.setAuthCache(authCache);
    }

    public String getBranchesForBuild(String buildKey) throws IOException {

        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credentialsProvider)
                .build();

        try {
            HttpGet httpget = new HttpGet(String.format("%splan/%s/branch", baseUrl, buildKey));
            log.info("Executing request {}", httpget.getRequestLine());
            HttpResponse response = httpclient.execute(httpget, context);
            HttpEntity entity = response.getEntity();
            String payload = EntityUtils.toString(entity);
            return payload;
        } finally {
            httpclient.close();
        }
    }

    public void triggerBambooBuild(String branchBuildKey) throws IOException {

        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credentialsProvider)
                .build();

        try {
            HttpPost httppost = new HttpPost(String.format("%squeue/%s", baseUrl, branchBuildKey));
            log.info("Executing request: {}", httppost.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httppost, context);
            try {
                log.info(String.valueOf(response.getStatusLine()));
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }
}
